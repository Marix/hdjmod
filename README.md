Linux driver for Hercules DJ devices
====================================

This provides a kernel module for using [Hercules DJ devices](https://www.hercules.com) on Linux.
It is a fork of the drivers originally provided by Hercules at <http://ts.hercules.com/ger/index.php?pg=view_files&gid=17&fid=62&pid=177&cid=1>.
It has been adapted to work on modern Linux kernels and should work on any current Linux distribution up to the latest released Kernel.

Compilation and Installation
----------------------------

On a somewhat standard Linux set-up, the module can be compiled the following:

	make modules

To install the built modules simply run the following:

	make install

If you system uses a non-standard location for the kernel sources, you can specify them via the `KERNELDIR` environment variable.

On openSUSE I'd recommend installing the driver from the official repositories:

	zypper install hdjmod-kmp-default
