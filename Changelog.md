Changelog
=========

This log lists the significant changes in each version of the hdjmod.

Version 1.36
------------

* Support Kernel 6.12

Version 1.35
------------

* Support compilation with GCC 14

Version 1.34
------------

* Support Kernel 6.4

Version 1.33
------------

* Support Kernel 5.19

Version 1.32
------------

This is the first version post 1.28 in this fork. The reason for the jump is that Stephane List's fork was already up to version 1.31 when those changes where integrated.

* Consolidates fixes made for openSUSE and Ubuntu packages, providing support for newer kernel up to including Linux 5.12.
* Add support for the Hercules Steel controller.

Version 1.28
------------

This is the original version published by Guillemot Corporation S.A. that could be downloaded at <http://ts.hercules.com/eng/index.php?pg=view_files&gid=2&fid=28&pid=177&cid=1>.
