/*
*
*  Copyright (c) 2010  Stephane List <slist@lilotux.net>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/

#if !defined(_USB_DEPRECATED_H_)
#define _USB_DEPRECATED_H_

#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,35) )
#include "linux/slab.h"

#define URB_NO_SETUP_DMA_MAP 0

static inline void *usb_buffer_alloc(struct usb_device *dev, size_t size,
				     gfp_t mem_flags, dma_addr_t *dma)
{
	return usb_alloc_coherent(dev, size, mem_flags, dma);
}
static inline void usb_buffer_free(struct usb_device *dev, size_t size,
				   void *addr, dma_addr_t dma)
{
	return usb_free_coherent(dev, size, addr, dma);
}



#endif
#endif
